var page = require('page');
var empty = require('empty-element');
var dash = require('../dashboard');
var template = require('./template');
var title = require('title');
var request = require('superagent');

page('/', sesion, obtenerComics, function (ctx, next) {
  title("Ecómic");
  var main = document.getElementById('main');
  empty(main).appendChild(dash(template(ctx.comics)));
})

function obtenerComics(ctx, next) {
  if(localStorage.comics == undefined){
    request
    .get('http://jsonplaceholder.typicode.com/posts')
    .end(function (err, res) {
      if (err) return console.log(err);

      ctx.comics = res.body;
      localStorage.comics = JSON.stringify(ctx.comics);
      next();
      
    })
  }else{
    var comics = JSON.parse(localStorage.comics);
    ctx.comics = comics;
    next();
    
  }
}

function sesion(ctx,next){
    if(localStorage.login == "" || localStorage.login == undefined){ window.location.href = "/signin"; }else{ next();}
}

