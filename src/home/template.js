var yo = require('yo-yo');
var buscador = require('../buscador');

module.exports  = function(comics){

    function comic(data){
        return yo`<div class="card col-6">
                <div class="card-title">${data.title}</div><br>
                    <p>${data.body}<p>
                    <a href="/comic/${data.id}">Ver mas</a>
            </div>`;
    }
    
    var template = yo`<div>
                <div>${buscador(comic)}</div>
                <div id="comics">
                ${comics.map(function (c) {
                return comic(c);
                })}
                </div>
            </div>`;
    
    return template;

}





