var yo = require('yo-yo');
var empty = require('empty-element');

module.exports = function(fn_comic){

    var buscador = yo`<div class="buscador">
                        <form>
                            <input onkeyup=${buscar} type="text" id="text-busqueda" placeholder="Buscar cómics"/>
                        </form>
                        <buttom onclick=${toggleAdd} class="buttom">Nuevo Cómic</buttom>
                        <div id="adicionar" class="card margen-superior hide">
                            <div class="card-title">Agregar Cómic</div>
                            <div class="contenido">
                                <form onsubmit=${agregar}>
                                    <input id="titulo" type="text" placeholder="Titulo" required />
                                    <input id="descripcion" type="text"  placeholder="Descripcion" required />
                                    <input type="submit" class="buttom" value="Agregar" />
                                </form>
                            </div>
                        </div>
                      </div>`;

        function buscar(){
            var text = document.getElementById('text-busqueda').value;
            console.log(text);
            var search = [];
            var comics = JSON.parse(localStorage.comics);
            if(text != ""){
                var rex = new RegExp(text)
                search = comics.filter( comic => {
                    if(rex.test(comic.title) || rex.test(comic.body)){
                        return comic;
                    }
                })
            }else{
                search = comics;
            }
            var template_search = yo`<div> ${search.map(function (c) {
                                            return fn_comic(c);
                                    })}</div>`;
            var container = document.getElementById('comics');
            empty(container).innerHTML = template_search.innerHTML;
        }

        function toggleAdd() {
            document.getElementById('adicionar').classList.toggle('hide');
        }

        function agregar(ev){
            ev.preventDefault();
            var comic = {}
            var comics = JSON.parse(localStorage.comics);
            comic.title = document.getElementById('titulo').value;
            comic.body = document.getElementById('descripcion').value;
            comic.id = comics.length + 1;
            var el = document.getElementById('comics');
            child = document.createElement('div');
            child.innerHTML = `<div class="card col-6">
                        <div class="card-title">${comic.title}</div><br>
                            <p>${comic.body}<p>
                            <a href="/comic/${comic.id}">Ver mas</a>
                    </div>`;
            el.insertBefore(child, el.firstChild);
            comics.unshift(comic);
            localStorage.comics = JSON.stringify(comics);
            toggleAdd();
            document.getElementById('descripcion').value = "";
            document.getElementById('titulo').value = "";
        }

    return buscador;

}
