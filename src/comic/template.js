var yo = require('yo-yo');

module.exports  = function(datacomic, comments){

    function comic(data){
        return yo`<div class="card col-6">
                <div class="card-title">${data.title}</div><br>
                    <p>${data.body}<p>
            </div>`;
    }

    function comentario(comment){
        return yo`<div class="comment">
                        <div class="autor">${comment.name} - ${comment.email}</div>
                        <div class="body">${comment.body}</div>
                    </div>`;
    }
    
    return yo`<div>
                ${comic(datacomic)}
                <h2>Comentarios</h2>
                <div class="comments">
                ${ comments.map(com => {
                    return comentario(com);
                })}
                </div>
            </div>`;

}