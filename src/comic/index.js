var page = require('page');
var empty = require('empty-element');
var template = require('./template');
var title = require('title');
var request = require('superagent');
var dash = require('../dashboard');


page('/comic/:id', sesion, getComic, getComments, function (ctx, next) {
  var main = document.getElementById('main');
  title(`Cómic - ${ctx.comic.id}`);
  empty(main).appendChild(dash(template(ctx.comic, ctx.comments)));
});

function sesion(ctx,next){
    if(localStorage.login == "" || localStorage.login == undefined){ window.location.href = "/signin"; }
    next();
}

function getComic(ctx, next) {
    request
    .get(`http://jsonplaceholder.typicode.com/posts/${ctx.params.id}`)
    .end(function (err, res) {
    if (err) { 
        console.log(err); 
        var comics = JSON.parse(localStorage.comics);
        var comic = comics.filter( c => {
            if(c.id == ctx.params.id){
                return c;
            }
        });
         ctx.comic = comic[0];
         ctx.comments = [];
         next();
     }else{
          ctx.comic = res.body; 
          next();
     } 
    })
}

function getComments(ctx, next){
     request
        .get(`http://jsonplaceholder.typicode.com/posts/${ctx.params.id}/comments`)
        .end(function (err, res) {
            if (err) return console.log(err);
                ctx.comments = res.body;
                next();
        })
}

