var yo = require('yo-yo');

module.exports = function () {
     var form =  yo`<div>
            <h1 class="logotipo">ECÓMICS</h1>
            <div class="card margen-superior">
                <div class="card-title">Inicio Sesión</div>
                <div class="contenido">
                    <form onsubmit=${login}>
                        <input id="username" type="text" placeholder="Nombre usuario" required />
                        <input id="password" type="password"  placeholder="Contraseña" required />
                        <input type="submit" class="buttom" value="Entrar" />
                    </form>
                </div>
            </div>
        </div>`;

        function login(ev) {
            ev.preventDefault();
            debugger;
            var name = document.getElementById('username').value; 
            var pass = document.getElementById('password').value;
            var users = JSON.parse(localStorage.users);
            if(users.length == 0){
                alert("El usuario no existe");
            }else{
                var user = users.filter( user => {
                    if(user.username == name && user.password == pass){ return user };
                });
                if(user.length == 0){
                    alert("El usuario no existe");
                }else{
                    localStorage.login = JSON.stringify(user);
                    window.location.href = "/";
                }
            }
        }

        return form;
}                  