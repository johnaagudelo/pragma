var page = require('page');
var empty = require('empty-element');
var template = require('./template');
var title = require('title');

page('/signin', session, createuser, function (ctx, next) {
  title("Ecómic - Iniciar Sesión");
  var main = document.getElementById('main');
   console.log(template());
  empty(main).appendChild(template());
})

function session(ctx, next){
    if(localStorage.login != undefined){
        var session = localStorage.login;
        if(session != ""){
          window.location.href = "/";
        }
    }
    next();
}

function createuser(ctx, next){
  if(localStorage.users == undefined){
      localStorage.users = `[
      { "username": "admin", "password": "admin", "name":"John Alexander Agudelo" }
    ]`;
  }
   next();
}