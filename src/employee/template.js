var yo = require('yo-yo');

module.exports = function(){
    var state = false;
    var reg = yo`<div>
                <div class="card margen-superior">
                    <div class="card-title">Registro</div>
                    <div class="contenido">
                        <span id="mensaje"></span>
                        <form onsubmit=${registro}>
                            <input type="text"  id="username" placeholder="Nombre usuario" required />
                            <input type="text"  id="name" placeholder="Nombre Completo" required />
                            <input type="password" onkeyup=${pass} id="password" placeholder="Contraseña" required />
                            <input type="submit" class="buttom" value="Registrar" />
                        </form>
                    </div>
                </div>
            </div>`;

            function pass(){
                debugger;
                rex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
                pass = document.getElementById('password').value;
                 if(!rex.test(pass)){
                     document.getElementById('password').classList.add("error");
                     document.getElementById('mensaje').innerHTML = "La contraseña debe contener Mayusculas, minusculas, números y una longitud de 8 a 15 caracteres";
                     state = false;
                 }else{
                    document.getElementById('password').classList.remove("error");
                     document.getElementById('mensaje').innerHTML = "";
                     state = true;
                 }
            }

            function registro(ev){
                ev.preventDefault();
                var employee = {};
                employee.username = document.getElementById('username').value;
                employee.password = document.getElementById('password').value;
                employee.name = document.getElementById('name').value;
                rex = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
                if(state){
                    var users = JSON.parse(localStorage.users);
                    users.push(employee);
                    localStorage.users = JSON.stringify(users);
                    document.getElementById('username').value = "";
                    document.getElementById('name').value = "";
                    document.getElementById('password').value = "";
                    document.getElementById('mensaje').innerHTML = "Empleado Registrado";
                }
               
            }
            
        return reg;
}

                    