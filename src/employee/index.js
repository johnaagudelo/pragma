var page = require('page');
var empty = require('empty-element');
var dash = require('../dashboard');
var template = require('./template');
var title = require('title');

page('/employee', sesion, function (ctx, next) {
  title("Ecómic - Registro");
  var main = document.getElementById('main');
  empty(main).appendChild(dash(template()));
})

function sesion(ctx,next){
    if(localStorage.login == ""){ window.location.href = "/signin"; }
    next();
}