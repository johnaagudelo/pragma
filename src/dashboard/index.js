var yo = require('yo-yo');

var template = function home(content){
    var template = yo`<div><header id="header" class="header contenedor">
                        <h1 class="logotipo"> 
                            ECÓMICS
                        </h1>
                        <span onclick=${menu_mobil} class="con-menu"><i class="fa fa-bars" aria-hidden="true"></i></span>
                        <nav class="menu" id="menu"> 
                            <ul>
                                <li>
                                <a href="/">Home</a>
                                </li>
                                <li>
                                <a href="/employee">Empleados</a>
                                </li>
                                <li>
                                <a onclick=${salir}>Salir</a>
                                </li>
                            </ul>
                        </nav>
                    </header>
                    ${content}
                    </div>`
        
        function salir(){
            localStorage.login = "";
            window.location.href = "/signin";
        }

        function menu_mobil(){
             document.getElementById('menu').style.left = '0%';
        }

        return template;
}

module.exports = template;