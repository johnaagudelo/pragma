var express = require('express');

var app = express();

app.set('view engine', 'pug');

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.render('index');
})

app.get('/employee', function (req, res) {
  res.render('index', { "title": "Registro de empleados" });
})

app.get('/signin', function (req, res) {
  res.render('index', { "title": "Iniciar Sesión" });
})

app.get('/comic/:id', function (req, res) {
  res.render('index', { title: `Ecómic - ${req.params.id}` });
})

app.listen(2000, function (err) {
  if (err) return console.log('Ocurrio un error'), process.exit(1);

  console.log('Ecomic esta corriendo por el puerto 2000');
})