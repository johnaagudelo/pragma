var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var babel = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

gulp.task('style',function(){
    gulp
        .src('index.scss')
        .pipe(sass())
        .pipe(rename('style.css'))
        .pipe(gulp.dest('public/css'));
})

gulp.task('assets', function(){
    gulp
        .src('assets/images/*')
        .pipe(gulp.dest('public/images'))
})

gulp.task('scripts',function(){
    browserify('./src/index.js')
        .transform(babel)
        .bundle()
        .pipe(source('index.js'))
        .pipe(rename('app.js'))
        .pipe(gulp.dest('public'))
})

gulp.task('default', ['style','assets','scripts'])